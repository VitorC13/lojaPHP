<?php
/**
 * Created by PhpStorm.
 * User: vitorcardoso
 * Date: 09/02/18
 * Time: 12:27
 */
function listaCategorias($conexao) {
    $categorias = array();
    $resultado = mysqli_query($conexao, "select * from categorias");

    while ($categoria = mysqli_fetch_assoc($resultado)){
        array_push($categorias,$categoria);
    }

    return $categorias;
}